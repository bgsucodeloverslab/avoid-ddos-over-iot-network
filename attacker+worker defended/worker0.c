#include "contiki.h"
#include "lib/list.h"
#include "lib/memb.h"
#include "lib/random.h"
#include "net/rime.h"
#include <stdio.h>
#include <string.h>
//receive and send unicast 


struct unicast_message {
  uint8_t type;
 
};
// two types of unicast message could be received
enum {
	UNICAST_TYPE_REJECTED,
	UNICAST_TYPE_SERVED
};
// define the maximun number of record can be hold on the list
#define MAX_RECORD 10
// Record structure hold the records
struct record 
{
	struct record *next;
	rimeaddr_t addr;
	char* message;
};
// defines a memory pool from which we allocat new record entries
MEMB(record_memb, struct record, MAX_RECORD);
// the record_list is a Contiki list that holds the records of requesting motes
LIST(record_list);

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
// whenever received an unicast message
static void
recv_uc(struct unicast_conn *c, const rimeaddr_t *from)
{    
int found = 0;	
// serve request section
	printf("Request received from %d.%d\n", from->u8[0], from->u8[1]);	
	// copy the message content to a temp char array
	char* temp;
	temp = packetbuf_dataptr();	
printf("Packet content: '%s'\n", temp);  
	
	struct record *n;	
	//list_init(record_list);
	// check through the current record list to see if find the address of the requesting 
for(n = list_head(record_list); n != NULL; n = n->next) 
{
//printf("entering for loop\n");
    // if found match further check if content is the same
    if(rimeaddr_cmp(&n->addr, from)) 
{printf("found record\n");
found = 1;
      // malicious
	  if(strcmp(temp, n->message) == 0)
	  {
		// decide not to serve
		printf("Request rejected.\n");	
		struct unicast_message *msg;
		msg = packetbuf_dataptr();	
		msg->type = UNICAST_TYPE_REJECTED;
		unicast_send(c, from);
	  }
		// when message is different, and has not been reported as malicious
	  else
		// provide service
		{		
		n->message = temp;
		printf("Serving request...");
		struct unicast_message *msg;
		msg = packetbuf_dataptr();
		msg->type = UNICAST_TYPE_SERVED;
		unicast_send(c, from);
		}
}}
	// record not exist in the list
  if(n == NULL && found == 0) 
  {
printf("allocating new record\n");
    n = memb_alloc(&record_memb);
    // initialize fields for the new record
    rimeaddr_copy(&n->addr, from);
    n->message = temp;
    // place record in the record list
    list_add(record_list, n);
	// provide service
	printf("Serving request...");
	struct unicast_message *msg;
	msg = packetbuf_dataptr();
	msg->type = UNICAST_TYPE_SERVED;
	unicast_send(c, from);
  
}//end if
//end for
printf("list length is %d\n", list_length(record_list));
}
static const struct unicast_callbacks unicast_callbacks = {recv_uc};
static struct unicast_conn uc;
PROCESS(unicast_process, "Serve request");
AUTOSTART_PROCESSES(&unicast_process);

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(unicast_process, ev, data)
{
  PROCESS_EXITHANDLER(unicast_close(&uc);)
    
  PROCESS_BEGIN();

  unicast_open(&uc, 146, &unicast_callbacks);

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
