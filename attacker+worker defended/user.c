#include "contiki.h"
#include "net/rime.h"
#include "dev/button-sensor.h"
#include "dev/leds.h"
#include <stdio.h>


// structure of unicast 
struct unicast_message
{
  uint8_t type;
};
// two types of unicast message could be received
enum
{
	UNICAST_TYPE_REJECTED,
	UNICAST_TYPE_SERVED
};
/*---------------------------------------------------------------------------*/
PROCESS(user_request_process, "Legitimate user request");
AUTOSTART_PROCESSES(&user_request_process);
/*---------------------------------------------------------------------------*/
static void
recv_uc(struct unicast_conn *c, const rimeaddr_t *from)
{
	struct unicast_message *msg;
	msg = packetbuf_dataptr();
	if(msg->type == UNICAST_TYPE_REJECTED)
	{
		printf("Request not served.\n");
		unicast_send(&c, from);
	}		
	else if(msg->type == UNICAST_TYPE_SERVED)
		printf("Request has been served by mote %d.%d:\n", from->u8[0], from->u8[1]);
	//printf("Respond received from mote %d.%d: '%s'\n", from->u8[0], from->u8[1],
	//		(char *)packetbuf_dataptr());	
	// handle the situation that the requeste node is busy
	//string is_busy = "BUSY";
	// using string compare
	// return 0 as equal 
	// upon the reply "BUSY", resend a request to the same working mote
	//if(!strcmp((char *)packetbuf_dataptr(), is_busy))
	//	unicast_send(&uc, from);
}
static const struct unicast_callbacks unicast_callbacks = {recv_uc};
static struct unicast_conn uc;
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(user_request_process, ev, data)
{
	PROCESS_EXITHANDLER(unicast_close(&uc);)
	PROCESS_BEGIN();
	unicast_open(&uc, 146, &unicast_callbacks);
	
	static struct etimer et;
	rimeaddr_t addr;
	
	etimer_set(&et, CLOCK_SECOND * 10);
	PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
	
	packetbuf_copyfrom("User requesting...", 18);
	// referring specific address of certain working mote
	addr.u8[0] = 1;
	addr.u8[1] = 0;
	// when the requested node address is different from the requesting node itself
	// do the unicast requesting
	if(!rimeaddr_cmp(&addr, &rimeaddr_node_addr)) {
      unicast_send(&uc, &addr);
    }
	  PROCESS_END();
}
