#include "contiki.h"
#include "lib/list.h"
#include "lib/memb.h"
#include "lib/random.h"
#include "net/rime.h"
#include <stdio.h>
//receive and send unicast 
struct unicast_message {
  uint8_t type;
};
// two types of unicast message could be received
enum {
	UNICAST_TYPE_REJECTED,
	UNICAST_TYPE_SERVED
};
/*---------------------------------------------------------------------------*/
PROCESS(unicast_process, "Serve request");
AUTOSTART_PROCESSES(&unicast_process);
/*---------------------------------------------------------------------------*/
// whenever received an unicast message
static void
recv_uc(struct unicast_conn *c, const rimeaddr_t *from)
{
      printf("Request received from %d.%d: '%s'\n", from->u8[0], from->u8[1], (char *)packetbuf_dataptr());
	  
	struct unicast_message *msg;
	  msg = packetbuf_dataptr();
	  
    msg->type = UNICAST_TYPE_SERVED;
    unicast_send(c, from);

}
static const struct unicast_callbacks unicast_callbacks = {recv_uc};
static struct unicast_conn uc;
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(unicast_process, ev, data)
{
  PROCESS_EXITHANDLER(unicast_close(&uc);)
    
  PROCESS_BEGIN();

  unicast_open(&uc, 146, &unicast_callbacks);

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
